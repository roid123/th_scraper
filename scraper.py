#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import requests
import time
from bs4 import BeautifulSoup

TOP_DOMAIN = 'https://ameblo.jp'
BLOG_URL = 'https://ameblo.jp/tomatsuharuka-blog/entrylist.html'
ARTICLE_DIR = 'articles'
SLEEP_SEC = 0.1


def save_article(article_url):
    r = requests.get(article_url)

    soup = BeautifulSoup(r.text, 'lxml')
    data = json.loads(soup.find('script', type="application/ld+json").string)

    title = data['headline']
    date_published = data['datePublished']

    content_tags = soup.find('div', attrs={
        'data-uranus-component': 'entryBody'
    }).contents
    contents = '\n'.join([str(tag) for tag in content_tags])

    a_dict = {
        'title': title,
        'date': date_published,
        'contents': contents,

    }

    file_path = '{}/{}.json'.format(ARTICLE_DIR, date_published)
    with open(file_path, 'w') as f:
        json.dump(a_dict, f, ensure_ascii=False)


def fetch_all_articles():
    if not os.path.exists(ARTICLE_DIR):
        os.mkdir(ARTICLE_DIR)

    current_link = BLOG_URL
    while True:
        r = requests.get(current_link)
        soup = BeautifulSoup(r.text, 'lxml')

        for h in soup.find_all('h2'):
            article_url = TOP_DOMAIN + h.a['href']
            print("saving {}".format(article_url))
            save_article(article_url)

        next_link = soup.find(attrs={
            'data-uranus-component': 'paginationNext'
        }).get('href')
        next_link = TOP_DOMAIN + next_link

        # 次のリンクと現在のリンクが一致したら最後なので抜ける
        if current_link == next_link:
            break
        current_link = next_link

        # 負荷をかけないように 0.1 秒待つ
        time.sleep(SLEEP_SEC)


if __name__ == '__main__':
    fetch_all_articles()
