# 戸松遥オフィシャルブログ スクレイピングツール

## Requires
- Python3
- Pipenv

## Usage
1. Pipfile の内容をインストール  
    ```
    $ pipenv install
    ```
1. Pipenv shell を有効化  
    ```
    $ pipenv shell
    ```
1. スクリプトを実行  
    ```
    $ python scraper.py
    ```
    - articles ディレクトリに日付ごとの JSON が保存される
